#!/usr/bin/python2

from distutils.core import setup, Extension

schedutils = Extension('schedutils',
		       sources = ['python-schedutils/schedutils.c'])

# don't reformat this line, Makefile parses it
setup(name='schedutils',
      version='0.6',
      description='Python module to interface with the Linux scheduler',
      author='Arnaldo Carvalho de Melo',
      author_email='acme@redhat.com',
      url='http://fedoraproject.org/wiki/python-schedutils',
      scripts=['pchrt', 'ptaskset'],
      data_files=[('share/man/man1', ['pchrt.1','ptaskset.1'])],
      ext_modules=[schedutils])

%if 0%{?fedora}
%global with_python3 1
%else
%global without_python3 1
%endif

Summary: Linux scheduler python bindings
Name: python-schedutils
Version: 0.6
Release: 1%{?dist}
License: GPLv2
URL: https://rt.wiki.kernel.org/index.php/Tuna
Source: https://cdn.kernel.org/pub/software/libs/python/%{name}/%{name}-%{version}.tar.xz
Group: System Environment/Libraries
BuildRequires: python2-devel

%if 0%{?with_python3}
BuildRequires: python3-devel
%endif

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global _description\
Python interface for the Linux scheduler sched_{get,set}{affinity,scheduler}
functions and friends.

%description %_description

%package -n python2-schedutils
Summary: %summary
%{?python_provide:%python_provide python2-schedutils}

%description -n python2-schedutils %_description

%if 0%{?with_python3}
%package -n python3-schedutils
Summary: %summary
%{?python_provide:%python_provide python3-schedutils}

%description -n python3-schedutils %_description
%endif

%prep
%autosetup -p1



%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif

%install
rm -rf %{buildroot}
%py2_install
%if 0%{?with_python3}
%py3_install
%endif

%clean
rm -rf %{buildroot}

%files -n python2-schedutils
%defattr(-,root,root)
%license COPYING
%{python2_sitearch}/schedutils.so
%license COPYING
%if 0%{?without_python3}
%{_bindir}/pchrt
%{_bindir}/ptaskset
%{_mandir}/man1/pchrt.1*
%{_mandir}/man1/ptaskset.1*
%endif
%{python2_sitearch}/*.egg-info

%if 0%{?with_python3}
%files -n python3-schedutils
%defattr(-,root,root)
%license COPYING
%{_bindir}/pchrt
%{_bindir}/ptaskset
%{_mandir}/man1/pchrt.1*
%{_mandir}/man1/ptaskset.1*
%{python3_sitearch}/schedutils*.so
%{python3_sitearch}/*.egg-info
%endif



%changelog
* Mon Nov 20 2017 Jiri Kastner <jkastner@redhat.com> - 0.6-1
- added python 3 support

* Thu Dec 22 2016 Jiri Kastner <jkastner@redhat.com> - 0.5-1
- added basic support for SCHED_DEADLINE
- fixed URL and Source in specfile

* Tue May 10 2016 John Kacur <jkacur@redhat.com> - 0.4-2
- Add man pages for pchrt and ptaskset
- Fix and update usage messages for pchrt and ptaskset

* Mon Aug  1 2011 Arnaldo Carvalho de Melo <acme@redhat.com> - 0.4-1
- New upstream release.

* Tue May 17 2011 Clark Williams <williams@redhat.com> - 0.3-1
- reworked get_affinity() and set_affinity() to use dynamic CPU_* macros

* Thu Aug 28 2008 Arnaldo Carvalho de Melo <acme@redhat.com> - 0.2-2
- Fix build and install sections as suggested by the fedora rewiewer
  (BZ #460387)

* Wed Aug 27 2008 Arnaldo Carvalho de Melo <acme@redhat.com> - 0.2-1
- Add get_priority_{min,max} methods
- Add constants for SCHED_{BATCH,FIFO,OTHER,RR}
- Implement get_priority method
- Add pchrt utility for testing the bindings, chrt clone
- Add ptaskset utility for testing the bindings, taskset clone

* Tue Jun 10 2008 Arnaldo Carvalho de Melo <acme@redhat.com> - 0.1-3
- add dist to the release tag

* Wed Dec 19 2007 Arnaldo Carvalho de Melo <acme@redhat.com> - 0.1-2
- First build into rhel5-rt

* Wed Dec 19 2007 Arnaldo Carvalho de Melo <acme@redhat.com> - 0.1-1
- Initial package
